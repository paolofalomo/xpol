module gitlab.com/paolofalomo/xpol

go 1.16

require (
	cloud.google.com/go v0.77.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	go.opencensus.io v0.22.6 // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777 // indirect
	golang.org/x/oauth2 v0.0.0-20210216194517-16ff1888fd2e // indirect
	golang.org/x/sys v0.0.0-20210216163648-f7da38b97c65 // indirect
	google.golang.org/api v0.40.0 // indirect
)
